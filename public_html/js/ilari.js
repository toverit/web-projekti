/*
 Created on : 26/03/2020
 Author     : Ilari Puustinen
 */

$(document).ready(function () {
    let quizzes = ['#quiz1', '#quiz2', '#quiz3', '#quiz4', '#quiz5', '#quiz6', '#quiz7', '#quiz8'];
    let visible;
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    //mitä tapahtuu kun painaa "Aloita" nappia
    $("#start").click(function () {
        visible = getRndInteger(0, quizzes.length - 1);
        $(quizzes[visible]).removeClass("not_visible");
        $(this).addClass("not_visible");
    });
    //valintanappien toiminta
    let correct = 0;
    $(".choice").click(function () {

        let attr = $(this).attr("name");
        let choice = "[name=" + attr + "]";
        $(choice).prop("disabled", true);
        let answer = Number($(this).val());
        if (answer === 1) {
            $(this).parent().addClass("selected").addClass("right");
            correct++;
        } else {
            $(this).parent().addClass("wrong");
            let attr = $(this).attr("name");
            let choice = "[name=" + attr + "][value=1]";
            $(choice).parent().addClass("right");
        }
        $("#" + attr).removeClass("not_visible");
        $("#next").removeClass("not_visible");
    });
    //seuraava-napin toiminta
    let total = 0;
    $("#next").click(function () {
        $(quizzes[visible]).addClass("not_visible");
        quizzes.splice(visible, 1);
        if (quizzes.length > 0) {

            visible = getRndInteger(0, quizzes.length - 1);
            $(quizzes[visible]).removeClass("not_visible");
            $(this).addClass("not_visible");
        } else {
            $(this).addClass("not_visible");
        }
        total++;
        $("#total").html(total);
        $("#tulos").html(correct);
        if (quizzes.length === 0) {
            $("#result").removeClass("not_visible");
            if (correct === 8) {
                $("#full").removeClass("not_visible");
            }
            if (correct === 6 || correct === 7) {
                $("#kehu").removeClass("not_visible");
            }
            if (correct === 4 || correct === 5) {
                $("#keski").removeClass("not_visible");
            }
            if (correct === 0 || correct <= 3) {
                $("#pettymys").removeClass("not_visible");
            }
        }

    });
    //matikkatesti
    let amount = 10;
    let oikein,
            done,
            cal1,
            cal2;
    //Aloita-nappi
    $("#math").click(function () {
        $("#math").addClass("not_visible");
        cal1 = getRndInteger(1, 10);
        cal2 = getRndInteger(1, 10);
        let sum = cal1 * cal2;
        oikein = sum;

        //laskutoimitus
        $("#lasku").text(cal1 + " * " + cal2 + " = ");
        $("#done").text(done);

        $("#answer").removeClass("not_visible");
        $("#check").removeClass("not_visible");
    });

    //Tarkistus-nappi
    $("#check").click(function () {
        let answer = Number($("#answer").val());

        //jos vastaus on oikein
        if (answer === oikein) {
            $("#jes").removeClass("not_visible");
            $("#seuraava").removeClass("not_visible");
            $("#check").addClass("not_visible");
            $("#väärin").addClass("not_visible");
            amount--;
            //jos vastaus on väärin
        } else if (answer !== oikein) {
            $("#jes").addClass("not_visible");
            $("#väärin").removeClass("not_visible");
        }
    });

    //Seuraava-nappi
    $("#seuraava").click(function () {
        $("#answer").text("");
        $("#jes").addClass("not_visible");
        $("#seuraava").addClass("not_visible");
        cal1 = getRndInteger(1, 10);
        cal2 = getRndInteger(1, 10);
        let sum = cal1 * cal2;
        oikein = sum;

        $("#lasku").text(cal1 + " * " + cal2 + " = ");
        $("#done").text(done);

        $("#answer").removeClass("not_visible");
        $("#check").removeClass("not_visible");

        //kun kaikkiin tehtäviin on vastattu
        if (amount === 0) {
            $("#result").removeClass("not_visible");
            $("#lasku").addClass("not_visible");
            $("#answer").addClass("not_visible");
            $("#check").addClass("not_visible");
        }
    });
});
