/*
 Created on : 26/03/2020
 Author     : Jami Salmela
 */
// visa
$(document).ready(function () {
    let quizzes = ['#quiz1', '#quiz2', '#quiz3', '#quiz4', '#quiz5', '#quiz6', '#quiz7', '#quiz8', '#quiz9', '#quiz10'];
    let visible = 0;
    let tnro = 0;
    let oikein = 0;

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    $("#start").click(function () {
        visible = getRndInteger(0, quizzes.length - 1);
        $(quizzes[visible]).removeClass("not_visible");
        $(this).addClass("not_visible");
        $('input[type="radio"]').prop('checked', false);
        tnro++;
        $("#numero").html(tnro);
        $("#alkuteksti").addClass("not_visible");

    });

    $(".choice").click(function () {

        let attr = $(this).attr("name");

        let choice = "[name=" + attr + "]";
        $(choice).prop("disabled", true);

        let answer = Number($(this).val());
        if (answer === 1) {
            $(this).parent().addClass("selected").addClass("right");
            oikein++;
        } else {
            $(this).parent().addClass("wrong");
            let attr = $(this).attr("name");
            let choice = "[name=" + attr + "][value=1]";
            $(choice).parent().addClass("right");
        }
        $("#" + attr).removeClass("not_visible");
        $("#next").removeClass("not_visible");
    });

// seuraava painike
    $("#next").click(function () {
        $(quizzes[visible]).addClass("not_visible");
        quizzes.splice(visible, 1);
        tnro++;
        $("#numero").html(tnro);
        $("#ans").html("");

        if (quizzes.length > 0) {
            visible = getRndInteger(0, quizzes.length - 1);
            $(quizzes[visible]).removeClass("not_visible");
            $(this).addClass("not_visible");

        } else {
            $(this).addClass("not_visible");
        }
        $("#oikein").html(oikein);
        if (tnro === 11) {
            if (oikein === 10) {
                $("#10").removeClass("not_visible");
            }
            if (oikein > 6 && oikein <= 9) {
                $("#789").removeClass("not_visible");
            }
            if (oikein > 3 && oikein <= 6) {
                $("#456").removeClass("not_visible");
            }
            if (oikein > 0 && oikein <= 3) {
                $("#123").removeClass("not_visible");
            }
            if (oikein < 1) {
                $("#0").removeClass("not_visible");
            }
            $("#alkuun").removeClass("not_visible");
        }
        $("#alkuun").click(function () {
            location.reload();
        });
    });


//    Matematiikka osuus
    function rndNumerot() {
        let nro = getRndInteger(11, 50);
        let nro2 = getRndInteger(1, 10);
        let oikea_vastaus = nro % nro2;
        tooo = oikea_vastaus;
        $("#numero1").html(nro);
        $("#numero2").html(nro2);

    }


//aloitus nappula
    $("#alotus").click(function () {
        $("#alotus").addClass("not_visible");
        $("#tehtava").removeClass("not_visible");
        $("#tarkistus").removeClass("not_visible");
        $("#laskuri").removeClass("not_visible");
        $("#tmäärä").addClass("not_visible");
        rndNumerot();
        $("#ateksti").addClass("not_visible");

        if ($("#t5").prop("checked") === true) {

            $("#montako").html("5");
        }
        if ($("#t10").prop("checked") === true) {

            $("#montako").html("10");
        }
        if ($("#t15").prop("checked") === true) {

            $("#montako").html("15");
        }
    });
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
//    Globaalit muuttujat

    let teht = 0;



//Tarkistukset
    $("#tarkistus").click(function () {
        let vastaus = Number($("#ans").val());
        if (vastaus === tooo) {
            $("#nästa").removeClass("not_visible");
            $("#oikeaVastaus").removeClass("not_visible");
        } else {
            $("#väärä").removeClass("not_visible");
        }
    });
//    Seuraava painike

    $("#nästa").click(function () {
        $("#nästa").addClass("not_visible");
        teht++;
        $("#ans").val("");
        $("#mnro").html(teht);
        $("#väärä").addClass("not_visible");
        $("#numero1").html("");
        $("#numero2").html("");
        rndNumerot();
        $("#oikeaVastaus").addClass("not_visible");

        if ($("#t5").prop("checked") === true) {

            if (teht === 5) {
                $("#tehtava").addClass("not_visible");
                $("#tarkistus").addClass("not_visible");
                $("#läpi").removeClass("not_visible");
                $("#alkuuun").removeClass("not_visible");
            }
        }
        if ($("#t10").prop("checked") === true) {
            if (teht === 10) {
                $("#tehtava").addClass("not_visible");
                $("#tarkistus").addClass("not_visible");
                $("#läpi").removeClass("not_visible");
                $("#alkuuun").removeClass("not_visible");
            }

        }
        if ($("#t15").prop("checked") === true) {
            if (teht === 15) {
                $("#tehtava").addClass("not_visible");
                $("#tarkistus").addClass("not_visible");
                $("#läpi").removeClass("not_visible");
                $("#alkuuun").removeClass("not_visible");
            }

        }

//testin alkuun
    });
    $("#alkuuun").click(function () {
        location.reload();
    });
});