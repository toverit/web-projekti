/*
    Created on : 26/03/2020
    Author     : Jukka Liimatta
*/

$(document).ready(function () {
    
    let vastausmäärä = 0;
    let oikeavastausmäärä = 0;
    $(".visa").click(function () {

        let vastaus = Number($(this).val());
        if (vastaus === 1) {
            $(this).parent().addClass("selected").addClass("right");
            vastausmäärä++;
            oikeavastausmäärä++;
        } else {
            $(this).parent().addClass("wrong");

            let attribute = $(this).attr("name");
            let choice = "[name=" + attribute + "][value=1]";
            $(choice).parent().addClass("right");
            vastausmäärä++;
        }
        let prosenttioikea = 100 / 8 * oikeavastausmäärä;
        $("#vastaus").html(vastausmäärä);
        $("#oikea").html(prosenttioikea + "%  ");
        
        if (vastausmäärä === 8 && prosenttioikea <= 25) {
            $("#yli").addClass('fa fa-frown-o');
        } else if (vastausmäärä === 8 && prosenttioikea >= 25 && prosenttioikea < 62){
            $("#yli").addClass('fa fa-meh-o');
        } else if (vastausmäärä === 8 && prosenttioikea >= 62 && prosenttioikea < 100){
            $("#yli").addClass('fa fa-smile-o');
        } else if (vastausmäärä === 8 && prosenttioikea === 100){
            $("#yli").addClass('fa fa-thumbs-up');
        }
    });
    
    $(".visa").click(function () {
        $(this).addClass("selected");
        let attribute = $(this).attr("name");
        let choice = "[name=" + attribute + "]";
        $(choice).prop("disabled", true);
    });

    let i = 0;
    $('.lilabel1').click(function () {
        if (i < 1) {
            $(".visa", this).triggerHandler('click');
            i++;
        }
    });
    let o = 0;
    $('.lilabel2').click(function () {
        if (o < 1) {
            $(".visa", this).triggerHandler('click');
            o++;
        }
    });
    let p = 0;
    $('.lilabel3').click(function () {
        if (p < 1) {
            $(".visa", this).triggerHandler('click');
            p++;
        }
    });
    let j = 0;
    $('.lilabel4').click(function () {
        if (j < 1) {
            $(".visa", this).triggerHandler('click');
            j++;
        }
    });
    let k = 0;
    $('.lilabel5').click(function () {
        if (k < 1) {
            $(".visa", this).triggerHandler('click');
            k++;
        }
    });
    let l = 0;
    $('.lilabel6').click(function () {
        if (l < 1) {
            $(".visa", this).triggerHandler('click');
            l++;
        }
    });
    let n = 0;
    $('.lilabel7').click(function () {
        if (n < 1) {
            $(".visa", this).triggerHandler('click');
            n++;
        }
    });
    let m = 0;
    $('.lilabel8').click(function () {
        if (m < 1) {
            $(".visa", this).triggerHandler('click');
            m++;
        }
    });
    
});
/* Matikkatehtävän javascript */
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

$(document).ready(function () {
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    /* Helppo */
    let rndnumeasy_plus1 = getRndInteger(2, 10);
    let rndanswereasy_plus1 = getRndInteger(11, 25);
    let rndnumeasy_plus2 = getRndInteger(2, 10);
    let rndanswereasy_plus2 = getRndInteger(11, 25);
    let rndnumeasy_plus3 = getRndInteger(2, 10);
    let rndanswereasy_plus3 = getRndInteger(11, 25);
    let rndnumeasy_plus4 = getRndInteger(2, 10);
    let rndanswereasy_plus4 = getRndInteger(11, 25);
    let rndnumeasy_minus1 = getRndInteger(11, 20);
    let rndanswereasy_minus1 = getRndInteger(1, 10);
    let rndnumeasy_minus2 = getRndInteger(11, 20);
    let rndanswereasy_minus2 = getRndInteger(1, 10);
    let rndnumeasy_minus3 = getRndInteger(11, 20);
    let rndanswereasy_minus3 = getRndInteger(1, 10);
    let rndnumeasy_minus4 = getRndInteger(11, 20);
    let rndanswereasy_minus4 = getRndInteger(1, 10);
    $("#helppo").click(function () {
        $(".taso").addClass("not_visible");
        $("#easy").addClass("visible");
        $("#nappi1").addClass("visible");

        $("#ea1").html(rndnumeasy_plus1);
        $("#ec1").html(rndanswereasy_plus1);

        $("#ea2").html(rndnumeasy_plus2);
        $("#ec2").html(rndanswereasy_plus2);

        $("#ea3").html(rndnumeasy_plus3);
        $("#ec3").html(rndanswereasy_plus3);

        $("#ea4").html(rndnumeasy_plus4);
        $("#ec4").html(rndanswereasy_plus4);

        $("#ea5").html(rndnumeasy_minus1);
        $("#ec5").html(rndanswereasy_minus1);

        $("#ea6").html(rndnumeasy_minus2);
        $("#ec6").html(rndanswereasy_minus2);

        $("#ea7").html(rndnumeasy_minus3);
        $("#ec7").html(rndanswereasy_minus3);

        $("#ea8").html(rndnumeasy_minus4);
        $("#ec8").html(rndanswereasy_minus4);
        
    });
    $("#nappi1").click(function(){
        let setNum1 = Number($("#eb1").val());
        let setNum2 = Number($("#eb2").val());
        let setNum3 = Number($("#eb3").val());
        let setNum4 = Number($("#eb4").val());
        let setNum5 = Number($("#eb5").val());
        let setNum6 = Number($("#eb6").val());
        let setNum7 = Number($("#eb7").val());
        let setNum8 = Number($("#eb8").val());
        
        let correctamount = 0;
        
        if (rndnumeasy_plus1 + setNum1 === rndanswereasy_plus1) {
            $("#ec1").addClass("correct");
            correctamount++;
        } else {
            $("#ec1").addClass("incorrect");
        }     
        if (rndnumeasy_plus2 + setNum2 === rndanswereasy_plus2) {
            $("#ec2").addClass("correct");
            correctamount++;
        } else {
            $("#ec2").addClass("incorrect");
        }      
        if (rndnumeasy_plus3 + setNum3 === rndanswereasy_plus3) {
            $("#ec3").addClass("correct");
            correctamount++;
        } else {
            $("#ec3").addClass("incorrect");
        }      
        if (rndnumeasy_plus4 + setNum4 === rndanswereasy_plus4) {
            $("#ec4").addClass("correct");
            correctamount++;
        } else {
            $("#ec4").addClass("incorrect");
        }
        
        
        if (rndnumeasy_minus1 - setNum5 === rndanswereasy_minus1) {
            $("#ec5").addClass("correct");
            correctamount++;
        } else {
            $("#ec5").addClass("incorrect");
        }    
        if (rndnumeasy_minus2 - setNum6 === rndanswereasy_minus2) {
            $("#ec6").addClass("correct");
            correctamount++;
        } else {
            $("#ec6").addClass("incorrect");
        }      
        if (rndnumeasy_minus3 - setNum7 === rndanswereasy_minus3) {
            $("#ec7").addClass("correct");
            correctamount++;
        } else {
            $("#ec7").addClass("incorrect");
        }     
        if (rndnumeasy_minus4 - setNum8 === rndanswereasy_minus4) {
            $("#ec8").addClass("correct");
            correctamount++;
        } else {
            $("#ec8").addClass("incorrect");
        }
        $(".määrä").addClass("visible");
        $("#oikeamäärä").html(correctamount);
        $("#nappi1").removeClass("visible");
        $("#back1").addClass("visible");
        
        if (correctamount === 8) {
            $("#star").addClass("fa fa-star-o");
        }
    });
    
    /* Vaativa */
    let rndnumadv_plus1 = getRndInteger(10, 20);
    let rndansweradv_plus1 = getRndInteger(25, 50);
    let rndnumadv_plus2 = getRndInteger(10, 20);
    let rndansweradv_plus2 = getRndInteger(25, 50);
    let rndnumadv_plus3 = getRndInteger(10, 20);
    let rndansweradv_plus3 = getRndInteger(25, 50);
    let rndnumadv_plus4 = getRndInteger(10, 20);
    let rndansweradv_plus4 = getRndInteger(25, 50);
    let rndnumadv_minus1 = getRndInteger(25, 50);
    let rndansweradv_minus1 = getRndInteger(1, 20);
    let rndnumadv_minus2 = getRndInteger(25, 50);
    let rndansweradv_minus2 = getRndInteger(1, 20);
    let rndnumadv_minus3 = getRndInteger(25, 50);
    let rndansweradv_minus3 = getRndInteger(1, 20);
    let rndnumadv_minus4 = getRndInteger(25, 50);
    let rndansweradv_minus4 = getRndInteger(1, 20);
    
    
    
    $("#vaativa").click(function () {
        $(".taso").addClass("not_visible");
        $("#advanced").addClass("visible");
        $("#nappi2").addClass("visible");

        $("#aa1").html(rndnumadv_plus1);
        $("#ac1").html(rndansweradv_plus1);
        
        $("#aa2").html(rndnumadv_plus2);
        $("#ac2").html(rndansweradv_plus2);
        
        $("#aa3").html(rndnumadv_plus3);
        $("#ac3").html(rndansweradv_plus3);
        
        $("#aa4").html(rndnumadv_plus4);
        $("#ac4").html(rndansweradv_plus4);
        
        $("#aa5").html(rndnumadv_minus1);
        $("#ac5").html(rndansweradv_minus1);
        
        $("#aa6").html(rndnumadv_minus2);
        $("#ac6").html(rndansweradv_minus2);

        $("#aa7").html(rndnumadv_minus3);
        $("#ac7").html(rndansweradv_minus3);
        
        $("#aa8").html(rndnumadv_minus4);
        $("#ac8").html(rndansweradv_minus4);
    });
    $("#nappi2").click(function(){
        let setNum1 = Number($("#ab1").val());
        let setNum2 = Number($("#ab2").val());
        let setNum3 = Number($("#ab3").val());
        let setNum4 = Number($("#ab4").val());
        let setNum5 = Number($("#ab5").val());
        let setNum6 = Number($("#ab6").val());
        let setNum7 = Number($("#ab7").val());
        let setNum8 = Number($("#ab8").val());
        
        let correctamount = 0;
        
        if (rndnumadv_plus1 + setNum1 === rndansweradv_plus1) {
            $("#ac1").addClass("correct");
            correctamount++;
        } else {
            $("#ac1").addClass("incorrect");
        }
        if (rndnumadv_plus2 + setNum2 === rndansweradv_plus2) {
            $("#ac2").addClass("correct");
            correctamount++;
        } else {
            $("#ac2").addClass("incorrect");
        }
        if (rndnumadv_plus3 + setNum3 === rndansweradv_plus3) {
            $("#ac3").addClass("correct");
            correctamount++;
        } else {
            $("#ac3").addClass("incorrect");
        }
        if (rndnumadv_plus4 + setNum4 === rndansweradv_plus4) {
            $("#ac4").addClass("correct");
            correctamount++;
        } else {
            $("#ac4").addClass("incorrect");
        }
        if (rndnumadv_minus1 - setNum5 === rndansweradv_minus1) {
            $("#ac5").addClass("correct");
            correctamount++;
        } else {
            $("#ac5").addClass("incorrect");
        }
        if (rndnumadv_minus2 - setNum6 === rndansweradv_minus2) {
            $("#ac6").addClass("correct");
            correctamount++;
        } else {
            $("#ac6").addClass("incorrect");
        }
        if (rndnumadv_minus3 - setNum7 === rndansweradv_minus3) {
            $("#ac7").addClass("correct");
            correctamount++;
        } else {
            $("#ac7").addClass("incorrect");
        }
        if (rndnumadv_minus4 - setNum8 === rndansweradv_minus4) {
            $("#ac8").addClass("correct");
            correctamount++;
        } else {
            $("#ac8").addClass("incorrect");
        }
        
        $("#nappi2").removeClass("visible");
        $("#back1").addClass("visible");
        $(".määrä").addClass("visible");
        $("#oikeamäärä").html(correctamount);
        
        if (correctamount === 8) {
            $("#star").addClass("fa fa-star-o");
        }
    });
    
    /* Hankala */
    let rndnumhard_plus1 = getRndInteger(30, 125);
    let rndanswerhard_plus1 = getRndInteger(150, 220);
    let rndnumhard_plus2 = getRndInteger(30, 125);
    let rndanswerhard_plus2 = getRndInteger(150, 220);
    let rndnumhard_plus3 = getRndInteger(30, 125);
    let rndanswerhard_plus3 = getRndInteger(150, 220);
    let rndnumhard_plus4 = getRndInteger(30, 125);
    let rndanswerhard_plus4 = getRndInteger(150, 220);
    let rndnumhard_minus1 = getRndInteger(220, 150);
    let rndanswerhard_minus1 = getRndInteger(25, 100);
    let rndnumhard_minus2 = getRndInteger(220, 150);
    let rndanswerhard_minus2 = getRndInteger(25, 100);
    let rndnumhard_minus3 = getRndInteger(220, 150);
    let rndanswerhard_minus3 = getRndInteger(25, 100);
    let rndnumhard_minus4 = getRndInteger(220, 150);
    let rndanswerhard_minus4 = getRndInteger(25, 100);
    
    
    $("#hankala").click(function () {
        $(".taso").addClass("not_visible");
        $("#hard").addClass("visible");
        $("#nappi3").addClass("visible");
        
        $("#ha1").html(rndnumhard_plus1);
        $("#hc1").html(rndanswerhard_plus1);
        
        $("#ha2").html(rndnumhard_plus2);
        $("#hc2").html(rndanswerhard_plus2);
        
        $("#ha3").html(rndnumhard_plus3);
        $("#hc3").html(rndanswerhard_plus3);
        
        $("#ha4").html(rndnumhard_plus4);
        $("#hc4").html(rndanswerhard_plus4);
        
        $("#ha5").html(rndnumhard_minus1);
        $("#hc5").html(rndanswerhard_minus1);
        
        $("#ha6").html(rndnumhard_minus2);
        $("#hc6").html(rndanswerhard_minus2);
        
        $("#ha7").html(rndnumhard_minus3);
        $("#hc7").html(rndanswerhard_minus3);
        
        $("#ha8").html(rndnumhard_minus4);
        $("#hc8").html(rndanswerhard_minus4);
    });
    $("#nappi3").click(function () {
        let setNum1 = Number($("#hb1").val());
        let setNum2 = Number($("#hb2").val());
        let setNum3 = Number($("#hb3").val());
        let setNum4 = Number($("#hb4").val());
        let setNum5 = Number($("#hb5").val());
        let setNum6 = Number($("#hb6").val());
        let setNum7 = Number($("#hb7").val());
        let setNum8 = Number($("#hb8").val());

        let correctamount = 0;
        
        if (rndnumhard_plus1 + setNum1 === rndanswerhard_plus1) {
            $("#hc1").addClass("correct");
            correctamount++;
        } else {
            $("#hc1").addClass("incorrect");
        }
        if (rndnumhard_plus2 + setNum2 === rndanswerhard_plus2) {
            $("#hc2").addClass("correct");
            correctamount++;
        } else {
            $("#hc2").addClass("incorrect");
        }
        if (rndnumhard_plus3 + setNum3 === rndanswerhard_plus3) {
            $("#hc3").addClass("correct");
            correctamount++;
        } else {
            $("#hc3").addClass("incorrect");
        }
        if (rndnumhard_plus4 + setNum4 === rndanswerhard_plus4) {
            $("#hc4").addClass("correct");
            correctamount++;
        } else {
            $("#hc4").addClass("incorrect");
        }
        if (rndnumhard_minus1 - setNum5 === rndanswerhard_minus1) {
            $("#hc5").addClass("correct");
            correctamount++;
        } else {
            $("#hc5").addClass("incorrect");
        }
        if (rndnumhard_minus2 - setNum6 === rndanswerhard_minus2) {
            $("#hc6").addClass("correct");
            correctamount++;
        } else {
            $("#hc6").addClass("incorrect");
        }
        if (rndnumhard_minus3 - setNum7 === rndanswerhard_minus3) {
            $("#hc7").addClass("correct");
            correctamount++;
        } else {
            $("#hc7").addClass("incorrect");
        }
        if (rndnumhard_minus4 - setNum8 === rndanswerhard_minus4) {
            $("#hc8").addClass("correct");
            correctamount++;
        } else {
            $("#hc8").addClass("incorrect");
        }
        $("#nappi3").removeClass("visible");
        $("#back1").addClass("visible");
        $(".määrä").addClass("visible");
        $("#oikeamäärä").html(correctamount);
        
        if (correctamount === 8) {
            $("#star").addClass("fa fa-star-o");
        }
    });
});