/*
 Created on : 26/03/2020
 Author     : Eveliina Purontaus
 */

$(document).ready(function () {
    let quizzes = ['#quiz1', '#quiz2', '#quiz3', '#quiz4', '#quiz5', '#quiz6', '#quiz7', '#quiz8'];
    let visible = 0;

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
// Visa-tehtävän ohjelmointi'
// 
// Aloitus-nappulan ohjelmointi
    $("#start").click(function () {
        // arpoo ensimmäisen kysymyksen
        visible = getRndInteger(0, quizzes.length - 1);
        $(quizzes[visible]).removeClass("not_visible");
        $(this).addClass("not_visible");
        $('input[type="radio"]').prop('checked', false);
    });

    let oikea = 0;

// Lukee mikä radio-nappuloista on valittuna ja kertoo, onko se oikea.
    $(".choice").click(function () {

        let attr = $(this).attr("name");

        let choice = "[name=" + attr + "]";
        $(choice).prop("disabled", true);

        // lukee, onko vastaus oikein vain väärin
        let answer = Number($(this).val());
        if (answer === 1) {
            $(this).parent().addClass("selected").addClass("right");
            oikea++;
        } else {
            $(this).parent().addClass("wrong");
            let attr = $(this).attr("name");
            let choice = "[name=" + attr + "][value=1]";
            $(choice).parent().addClass("right");
        }
        $("#" + attr).removeClass("not_visible");
        $("#next").removeClass("not_visible");
    });

    let total = 0;

// Seuraava-napin ohjelmointi
    $("#next").click(function () {
        $(quizzes[visible]).addClass("not_visible");
        quizzes.splice(visible, 1);

        // arpoo seuraavan kysymyksen
        if (quizzes.length > 0) {
            visible = getRndInteger(0, quizzes.length - 1);
            $(quizzes[visible]).removeClass("not_visible");
            $(this).addClass("not_visible");
        } else {
            $(this).addClass("not_visible");
        }
        total++;
        $("#total").html(total);
        $("#tulos").html(oikea);

        // tuo esiin kehun oikeiden vastausten mukaan.
        if (quizzes.length === 0) {
            $("#result").removeClass("not_visible");
            if (oikea === 8) {
                $("#kehua").removeClass("not_visible");
            } else if (oikea >= 4) {
                $("#kehu2").removeClass("not_visible");
            } else if (oikea >= 1) {
                $("#kehu3").removeClass("not_visible");
            } else if (oikea === 0) {
                $("#kehu4").removeClass("not_visible");
            }
        }
    });

// Matematiikkatehtävän ohjelmointi
    let oikein = 0;
    let määrä = 0;
    let tehty = 0;
    let lasku1 = 0;
    let lasku2 = 0;

// Aloita-nappulan ohjelmointi
    $("#aloita").click(function () {
        // lukee minkä tehtävämäärän oppilas on valinnut.
        if ($("#yksi").prop("checked") === true) {
            määrä = 10;
        } else if ($("#kaksi").prop("checked") === true) {
            määrä = 15;
        } else if ($("#kolme").prop("checked") === true) {
            määrä = 20;
        }
        
        // muodostaa laskun
        lasku1 = getRndInteger(1, 10);
        lasku2 = getRndInteger(1, 10);

        $(this).addClass("not_visible");

        let tulos = lasku1 + lasku2;
        oikein = tulos;

        // tulostaa laskun, tuo tarkitusnappulan esille, tyhjentää vastauskentän.
        $("#lasku").html(lasku1 + " + " + lasku2 + " = ");       
        $("#tehtynä").html(tehty);
        $("#vastaus").removeClass("not_visible");
        $("#tarkistus").removeClass("not_visible");
        $("#vastaus").val("");
    });

// Tarkistusnapin ohjelmointi
    $("#tarkistus").click(function () {
        let vastaus = Number($("#vastaus").val()); // lukee vastauksen
        $("#seuraava").addClass("not_visible"); // hävittää seuraava-nappulan
        
        if (vastaus === oikein) {
            $("#kehu").removeClass("not_visible");
            $("#seuraava").removeClass("not_visible");
            $("#toru").addClass("not_visible");
            tehty++;
            määrä--;
        } else if (vastaus !== oikein) {
            $("#toru").removeClass("not_visible");
            
        }
    });

// Seuraava-napin ohjelmointi
    $("#seuraava").click(function () {
        $("#kehu").addClass("not_visible");       
        $("#seuraava").addClass("not_visible");
        $("#vastaus").val("");  // tyhjentää vastauskentän
               
        // muodostaa uuden laskun
        lasku1 = getRndInteger(1, 10);
        lasku2 = getRndInteger(1, 10);
        let tulos = lasku1 + lasku2;
        oikein = tulos;

        $("#lasku").html(lasku1 + " + " + lasku2 + " = ");       
        $("#tehtynä").html(tehty);
        
        // testin lopetus, kun tehtävämäärä on tehty
        if (määrä === 0) {
            $("#valmis").removeClass("not_visible");
            $("#seuraava").addClass("not_visible");
            $("#tarkistus").addClass("not_visible");
            $("#lasku").addClass("not_visible");
        }
    });
});