/*
    Created on : 26/03/2020
    Author     : Henri Aukee
*/

$(document).ready(function () {
    let kysymykset = ["#quiz1", "#quiz2", "#quiz3", "#quiz4", "#quiz5", "#quiz6", "#quiz7", "#quiz8"];
    let visible = 0;
    visible = getRndInteger(0, kysymykset.length - 1);


    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    // Visa-tehtävä alkaa
    //Aloitus painike
    
    $("#start").click(function () {
        $(kysymykset[visible]).removeClass("not_visible");
        $(this).addClass("not_visible");
        $('input[type="radio"]').prop('checked', false);
    });

    let oikea = 0;

    $(".choice").click(function () {
        let attribuutinArvo = $(this).attr("name");

        let valinta = "[name=" + attribuutinArvo + "]";
        $(valinta).prop("disabled", true);

        let vastaus = Number($(this).val());

        if (vastaus === 1) {
            $(this).parent().addClass("selected").addClass("right");
            oikea++;
            $("#palkinto").removeClass("not_visible");

        } else {
            $(this).parent().addClass("wrong");

            let attribuutinArvo = $(this).attr("name");
            let valinta = "[name=" + attribuutinArvo + "][value=1]";
            $(valinta).parent().addClass("right");
        }

        $("#" + attribuutinArvo).removeClass("not_visible");
        $("#next").removeClass("not_visible");
    });


    let total = 0;
    //Seuraava nappi
    
    $("#next").click(function () {
        $("#palkinto").addClass("not_visible");
        $(kysymykset[visible]).addClass("not_visible");
        kysymykset.splice(visible, 1);

        if (kysymykset.length > 0) {
            visible = getRndInteger(0, kysymykset.length - 1);
            $(kysymykset[visible]).removeClass("not_visible");
            $(this).addClass("not_visible");
        } else {
            $(this).addClass("not_visible");
        }
        total++;
        $("#total").html(total);
        $("#tulos").html(oikea);

        // Tuo palkinnon esiin riippuen oikein vastattujen määrästä
        
        if (kysymykset.length === 0) {
            $("#result").removeClass("not_visible");
            if (oikea === 8) {
                $("#reward1").removeClass("not_visible");
            } else if (oikea >= 5) {
                $("#reward2").removeClass("not_visible");
            } else if (oikea >= 1) {
                $("#reward3").removeClass("not_visible");
            } else if (oikea === 0) {
                $("#reward4").removeClass("not_visible");
            }
            $("#Paluu").removeClass("not_visible");
        }
        $("#Paluu").click(function () {
            location.reload();
        });
    });


    // Matematiikkatehtävän alku

    let luku1 = 0;
    let luku2 = 0;
    let correct = 0;
    let amount = 0;
    let progress = 0;

// Aloitus nappi

    $("#begin").click(function () {
        // Lukee minkä tehtävämäärän oppilas on valinnut.
        
        if ($("#first").prop("checked") === true) {
            amount = 10;
        } else if ($("#second").prop("checked") === true) {
            amount = 15;
        } else if ($("#third").prop("checked") === true) {
            amount = 20;
        }

        // Laskun esiin muodostaminen
        
        luku1 = getRndInteger(1, 10);
        luku2 = getRndInteger(1, 10);

        $(this).addClass("not_visible");

        let product = luku1 * luku2;
        correct = product;

        // Tulostaa laskun, Tuo tarkitusnappulan esille, Tyhjentää vastauskentän.
        
        $("#calculation").html(luku1 + " * " + luku2 + " = ");
        $("#progress").html(progress);
        $("#answer").removeClass("not_visible");
        $("#check").removeClass("not_visible");
        $("#answer").val("");
    });

// Tarkistusnapin ohjelmointi

    $("#check").click(function () {
        let answer = Number($("#answer").val());
        $("#next").addClass("not_visible");

        if (answer === correct) {
            $("#reward").removeClass("not_visible");
            $("#next").removeClass("not_visible");
            $("#wrong").addClass("not_visible");
            progress++;
            amount--;
        } else if (answer !== correct) {
            $("#wrong").removeClass("not_visible");
        }
    });

// Seuraava-napin ohjelmointi

    $("#next").click(function () {
        $("#reward").addClass("not_visible");
        $("#next").addClass("not_visible");
        $("#answer").val("");  // Vastauskentän tyhjennys

        // Úuden laskun muodostaminen
        
        luku1 = getRndInteger(1, 10);
        luku2 = getRndInteger(1, 10);
        let product = luku1 * luku2;
        correct = product;

        $("#calculation").html(luku1 + " * " + luku2 + " = ");
        $("#progress").html(progress);

        // Testin lopetus, kun valittu tehtävä määrä on suoritettu
        
        if (amount === 0) {
            $("#done").removeClass("not_visible");
            $("#next").addClass("not_visible");
            $("#check").addClass("not_visible");
            $("#calculation").addClass("not_visible");
        }
    });
});
