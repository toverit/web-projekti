
/*
 * rakettipeli
 */


$(document).ready(function () {

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    let kerrat = 1;
    let eka = 0;
    let toka = 0;
    let oikea = 0;
    let yritykset = 0;


    function lasku() {

        $("#alkuRuutu").addClass("not_visible");
        $("#tehtävä").removeClass("not_visible");



        eka = Number(getRndInteger(2, 9));
        toka = Number(getRndInteger(1, 9) * eka);

        oikea = toka / eka;

        $("#kerta").html(kerrat + " /10");

        $("#vastaus").html(toka + " : " + eka + " =");

    }


    $("#tarkista").click(function () {
        let luku = Number($("#luku").val());
        yritykset++;
        if (luku === oikea) {
            $("[id=check]").removeClass("not_visible");
            $("[id=väärin]").addClass("not_visible");
            $("#rakettikuva").css({"margin-top": "-=17px"});
            $("[id=tarkista]").prop("disabled", true);
            $("[id=seuraava]").prop("disabled", false);
            $("[id=seuraava]").removeClass("ml-5");
            $("[id=seuraava]").addClass("ml-2");

        } else {

            $("#luku").focus();
            $("[id=väärin]").removeClass("not_visible");
        }
    });


    $("#seuraava").click(function () {
        kerrat++;
        $("[id=väärin]").addClass("not_visible");
        $("[id=check]").addClass("not_visible");
        $("[id=seuraava]").removeClass("ml-2");
        $("[id=seuraava]").addClass("ml-5");

        if (kerrat < 11) {
            $("#vastaus").html("");
            $("#tuomio").html("");
            $("[id=seuraava]").prop("disabled", true);
            $("#luku").focus();
            $("[id=tarkista]").prop("disabled", false);

            lasku();
        } else {

            $("[id=alkuRuutu]").addClass("not_visible");
            $("[id=kuvat]").addClass("not_visible");
            $("[id=lopetus]").removeClass("not_visible");
            $("[id=ruudut]").removeClass("col-6");
            $("[id=ruudut]").addClass("col-12");
            $("#tuloksia").html("Yrityksiä = " + yritykset);
            $("[id=tehtävä]").addClass("not_visible");

        }
    });

    $("#aloita").click(function () {

        lasku();



    });



    $("#uudestaan").click(function () {

        kerrat = 1;
        eka = 0;
        toka = 0;
        oikea = 0;
        yritykset = 0;

        $("#tehtävä").removeClass("not_visible");

        $("[id=kuvat]").removeClass("not_visible");
        $("[id=lopetus]").addClass("not_visible");
        $("[id=ruudut]").addClass("col-6");
        $("[id=ruudut]").removeClass("col-12");
        $("#rakettikuva").css({"margin-top": "130px"});
        $("[id=seuraava]").prop("disabled", true);
        $("#luku").focus();
        $("[id=tarkista]").prop("disabled", false);
        lasku();

    });

});





 