/*
 Created on : 26/03/2020
 Author     : Oskari Juntunen
 */

$(document).ready(function () {

    let visat = ["#visa1", "#visa2", "#visa3", "#visa4", "#visa5", "#visa6",
        "#visa7", "#visa8", "#visa9"];
    let näkyvä = 0;

    var pallot = ["#pallo1", "#pallo2", "#pallo3", "#pallo4", "#pallo5", "#pallo6",
        "#pallo7", "#pallo8", "#pallo9"];

    let luku = 0;

    var oikein = 0;

    $("#aloita").click(function () {

        $("#visa1").removeClass("not_visible");
        $("#alkuRuutu").addClass("not_visible");

    });

    $("#vastaus1").click(function () {

        if ($("#visa1a").is(":checked")) {
            luku++;
            $("[id=visa1a]").next().addClass("bg-danger", "text-white");
            $("[id=visa1b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus1]").prop("disabled", true);
            $("[id=seuraava1]").prop("disabled", false);
            $("[id=visa1b]").attr("disabled", "true");
            $("[id=visa1c]").attr("disabled", "true");
            $("[id=teksti1]").removeClass("blurry-text");
            $("[id=teksti1]").removeClass("disabled");
            $("[id=teksti1]").removeClass("valinta");


        } else if ($("#visa1b").is(":checked")) {
            luku++;
            $("[id=visa1b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "green");
            $("[id=vastaus1]").prop("disabled", true);
            $("[id=seuraava1]").prop("disabled", false);
            $("[id=visa1a]").attr("disabled", "true");
            $("[id=visa1c]").attr("disabled", "true");
            $("[id=teksti1]").removeClass("blurry-text");
            $("[id=teksti1]").removeClass("disabled");
            $("[id=teksti1]").removeClass("valinta");
            oikein++;

        } else if ($("#visa1c").is(":checked")) {
            luku++;
            $("[id=visa1c]").next().addClass("bg-danger", "text-white");
            $("[id=visa1b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus1]").prop("disabled", true);
            $("[id=seuraava1]").prop("disabled", false);
            $("[id=visa1a]").attr("disabled", "true");
            $("[id=visa1b]").attr("disabled", "true");
            $("[id=teksti1]").removeClass("blurry-text");
            $("[id=teksti1]").removeClass("disabled");
            $("[id=teksti1]").removeClass("valinta");

        } else {
            $('#virhe').modal('show')
        }


    });
    $("#seuraava1").click(function () {

        $("#visa2").removeClass("not_visible");
        $("#visa1").addClass("not_visible");
    });

    $("#vastaus2").click(function () {

        if ($("#visa2a").is(":checked")) {
            luku++;
            $("[id=visa2a]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "green");
            $("[id=vastaus2]").prop("disabled", true);
            $("[id=seuraava2]").prop("disabled", false);
            $("[id=visa2b]").attr("disabled", "true");
            $("[id=visa2c]").attr("disabled", "true");
            $("[id=teksti2]").removeClass("blurry-text");
            $("[id=teksti2]").removeClass("disabled");
            $("[id=teksti2]").removeClass("valinta");
            oikein++;

        } else if ($("#visa2b").is(":checked")) {
            luku++;
            $("[id=visa2b]").next().addClass("bg-danger", "text-white");
            $("[id=visa2a]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus2]").prop("disabled", true);
            $("[id=seuraava2]").prop("disabled", false);
            $("[id=visa2a]").attr("disabled", "true");
            $("[id=visa2c]").attr("disabled", "true");
            $("[id=teksti2]").removeClass("blurry-text");
            $("[id=teksti2]").removeClass("disabled");
            $("[id=teksti2]").removeClass("valinta");

        } else if ($("#visa2c").is(":checked")) {
            luku++;
            $("[id=visa2c]").next().addClass("bg-danger", "text-white");
            $("[id=visa2a]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus2]").prop("disabled", true);
            $("[id=seuraava2]").prop("disabled", false);
            $("[id=visa2a]").attr("disabled", "true");
            $("[id=visa2b]").attr("disabled", "true");
            $("[id=teksti2]").removeClass("blurry-text");
            $("[id=teksti2]").removeClass("disabled");
            $("[id=teksti2]").removeClass("valinta");

        } else {
            $('#virhe').modal('show')
        }



    });
    $("#seuraava2").click(function () {

        $("#visa3").removeClass("not_visible");
        $("#visa2").addClass("not_visible");
    });

    $("#vastaus3").click(function () {

        if ($("#visa3a").is(":checked")) {
            luku++;

            $("[id=visa3a]").next().addClass("bg-danger", "text-white");
            $("[id=visa3b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus3]").prop("disabled", true);
            $("[id=seuraava3]").prop("disabled", false);
            $("[id=visa3b]").attr("disabled", "true");
            $("[id=visa3c]").attr("disabled", "true");
            $("[id=teksti3]").removeClass("blurry-text");
            $("[id=teksti3]").removeClass("disabled");
            $("[id=teksti3]").removeClass("valinta");

        } else if ($("#visa3b").is(":checked")) {
            luku++;
            $("[id=visa3b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "green");
            $("[id=vastaus3]").prop("disabled", true);
            $("[id=seuraava3]").prop("disabled", false);
            $("[id=visa3a]").attr("disabled", "true");
            $("[id=visa3c]").attr("disabled", "true");
            $("[id=teksti3]").removeClass("blurry-text");
            $("[id=teksti3]").removeClass("disabled");
            $("[id=teksti3]").removeClass("valinta");
            oikein++;

        } else if ($("#visa3c").is(":checked")) {
            luku++;
            $("[id=visa3c]").next().addClass("bg-danger", "text-white");
            $("[id=visa3b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus3]").prop("disabled", true);
            $("[id=seuraava3]").prop("disabled", false);
            $("[id=visa3a]").attr("disabled", "true");
            $("[id=visa3b]").attr("disabled", "true");
            $("[id=teksti3]").removeClass("blurry-text");
            $("[id=teksti3]").removeClass("disabled");
            $("[id=teksti3]").removeClass("valinta");

        } else {
            $('#virhe').modal('show')
        }


    });
    $("#seuraava3").click(function () {

        $("#visa4").removeClass("not_visible");
        $("#visa3").addClass("not_visible");
    });

    $("#vastaus4").click(function () {

        if ($("#visa4a").is(":checked")) {
            luku++;

            $("[id=visa4a]").next().addClass("bg-danger", "text-white");
            $("[id=visa4b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus4]").prop("disabled", true);
            $("[id=seuraava4]").prop("disabled", false);
            $("[id=visa4b]").attr("disabled", "true");
            $("[id=visa4c]").attr("disabled", "true");
            $("[id=teksti4]").removeClass("blurry-text");
            $("[id=teksti4]").removeClass("disabled");
            $("[id=teksti4]").removeClass("valinta");

        } else if ($("#visa4b").is(":checked")) {
            luku++;
            $("[id=visa4b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "green");
            $("[id=vastaus4]").prop("disabled", true);
            $("[id=seuraava4]").prop("disabled", false);
            $("[id=visa4a]").attr("disabled", "true");
            $("[id=visa4c]").attr("disabled", "true");
            $("[id=teksti4]").removeClass("blurry-text");
            $("[id=teksti4]").removeClass("disabled");
            $("[id=teksti4]").removeClass("valinta");
            oikein++;

        } else if ($("#visa4c").is(":checked")) {
            luku++;
            $("[id=visa4c]").next().addClass("bg-danger", "text-white");
            $("[id=visa4b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus4]").prop("disabled", true);
            $("[id=seuraava4]").prop("disabled", false);
            $("[id=visa4a]").attr("disabled", "true");
            $("[id=visa4b]").attr("disabled", "true");
            $("[id=teksti4]").removeClass("blurry-text");
            $("[id=teksti4]").removeClass("disabled");
            $("[id=teksti4]").removeClass("valinta");

        } else {
            $('#virhe').modal('show')
        }


    });
    $("#seuraava4").click(function () {

        $("#visa5").removeClass("not_visible");
        $("#visa4").addClass("not_visible");
    });

    $("#vastaus5").click(function () {

        if ($("#visa5a").is(":checked")) {
            luku++;

            $("[id=visa5a]").next().addClass("bg-danger", "text-white");
            $("[id=visa5b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus5]").prop("disabled", true);
            $("[id=seuraava5]").prop("disabled", false);
            $("[id=visa5b]").attr("disabled", "true");
            $("[id=visa5c]").attr("disabled", "true");
            $("[id=teksti5]").removeClass("blurry-text");
            $("[id=teksti5]").removeClass("disabled");
            $("[id=teksti5]").removeClass("valinta");

        } else if ($("#visa5b").is(":checked")) {
            luku++;
            $("[id=visa5b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "green");
            $("[id=vastaus5]").prop("disabled", true);
            $("[id=seuraava5]").prop("disabled", false);
            $("[id=visa5a]").attr("disabled", "true");
            $("[id=visa5c]").attr("disabled", "true");
            $("[id=teksti5]").removeClass("blurry-text");
            $("[id=teksti5]").removeClass("disabled");
            $("[id=teksti5]").removeClass("valinta");
            oikein++;

        } else if ($("#visa5c").is(":checked")) {
            luku++;
            $("[id=visa5c]").next().addClass("bg-danger", "text-white");
            $("[id=visa5b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus5]").prop("disabled", true);
            $("[id=seuraava5]").prop("disabled", false);
            $("[id=visa5a]").attr("disabled", "true");
            $("[id=visa5b]").attr("disabled", "true");
            $("[id=teksti5]").removeClass("blurry-text");
            $("[id=teksti5]").removeClass("disabled");
            $("[id=teksti5]").removeClass("valinta");

        } else {
            $('#virhe').modal('show')
        }

    });
    $("#seuraava5").click(function () {

        $("#visa6").removeClass("not_visible");
        $("#visa5").addClass("not_visible");
    });

    $("#vastaus6").click(function () {

        if ($("#visa6a").is(":checked")) {
            luku++;

            $("[id=visa6a]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "green");
            $("[id=vastaus6]").prop("disabled", true);
            $("[id=seuraava6]").prop("disabled", false);
            $("[id=visa6b]").attr("disabled", "true");
            $("[id=visa6c]").attr("disabled", "true");
            $("[id=teksti6]").removeClass("blurry-text");
            $("[id=teksti6]").removeClass("disabled");
            $("[id=teksti6]").removeClass("valinta");
            oikein++;

        } else if ($("#visa6b").is(":checked")) {
            luku++;
            $("[id=visa6b]").next().addClass("bg-danger", "text-white");
            $("[id=visa6a]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus6]").prop("disabled", true);
            $("[id=seuraava6]").prop("disabled", false);
            $("[id=visa6a]").attr("disabled", "true");
            $("[id=visa6c]").attr("disabled", "true");
            $("[id=teksti6]").removeClass("blurry-text");
            $("[id=teksti6]").removeClass("disabled");
            $("[id=teksti6]").removeClass("valinta");

        } else if ($("#visa6c").is(":checked")) {
            luku++;
            $("[id=visa6c]").next().addClass("bg-danger", "text-white");
            $("[id=visa6a]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus6]").prop("disabled", true);
            $("[id=seuraava6]").prop("disabled", false);
            $("[id=visa6a]").attr("disabled", "true");
            $("[id=visa6b]").attr("disabled", "true");
            $("[id=teksti6]").removeClass("blurry-text");
            $("[id=teksti6]").removeClass("disabled");
            $("[id=teksti6]").removeClass("valinta");

        } else {
            $('#virhe').modal('show')
        }

    });
    $("#seuraava6").click(function () {

        $("#visa7").removeClass("not_visible");
        $("#visa6").addClass("not_visible");
    });

    $("#vastaus7").click(function () {

        if ($("#visa7a").is(":checked")) {
            luku++;

            $("[id=visa7a]").next().addClass("bg-danger", "text-white");
            $("[id=visa7c]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus7]").prop("disabled", true);
            $("[id=seuraava7]").prop("disabled", false);
            $("[id=visa7b]").attr("disabled", "true");
            $("[id=visa7c]").attr("disabled", "true");
            $("[id=teksti7]").removeClass("blurry-text");
            $("[id=teksti7]").removeClass("disabled");
            $("[id=teksti7]").removeClass("valinta");


        } else if ($("#visa7b").is(":checked")) {
            luku++;
            $("[id=visa7b]").next().addClass("bg-danger", "text-white");
            $("[id=visa7c]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus7]").prop("disabled", true);
            $("[id=seuraava7]").prop("disabled", false);
            $("[id=visa7a]").attr("disabled", "true");
            $("[id=visa7c]").attr("disabled", "true");
            $("[id=teksti7]").removeClass("blurry-text");
            $("[id=teksti7]").removeClass("disabled");
            $("[id=teksti7]").removeClass("valinta");

        } else if ($("#visa7c").is(":checked")) {
            luku++;
            $("[id=visa7c]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "green");
            $("[id=vastaus7]").prop("disabled", true);
            $("[id=seuraava7]").prop("disabled", false);
            $("[id=visa7a]").attr("disabled", "true");
            $("[id=visa7b]").attr("disabled", "true");
            $("[id=teksti7]").removeClass("blurry-text");
            $("[id=teksti7]").removeClass("disabled");
            $("[id=teksti7]").removeClass("valinta");
            oikein++;
        } else {
            $('#virhe').modal('show')
        }


    });
    $("#seuraava7").click(function () {

        $("#visa8").removeClass("not_visible");
        $("#visa7").addClass("not_visible");
    });

    $("#vastaus8").click(function () {

        if ($("#visa8a").is(":checked")) {
            luku++;

            $("[id=visa8a]").next().addClass("bg-danger", "text-white");
            $("[id=visa8b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus8]").prop("disabled", true);
            $("[id=seuraava8]").prop("disabled", false);
            $("[id=visa8b]").attr("disabled", "true");
            $("[id=visa8c]").attr("disabled", "true");
            $("[id=teksti8]").removeClass("blurry-text");
            $("[id=teksti8]").removeClass("disabled");
            $("[id=teksti8]").removeClass("valinta");

        } else if ($("#visa8b").is(":checked")) {
            luku++;
            $("[id=visa8b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "green");
            $("[id=vastaus8]").prop("disabled", true);
            $("[id=seuraava8]").prop("disabled", false);
            $("[id=visa8a]").attr("disabled", "true");
            $("[id=visa8c]").attr("disabled", "true");
            $("[id=teksti8]").removeClass("blurry-text");
            $("[id=teksti8]").removeClass("disabled");
            $("[id=teksti8]").removeClass("valinta");
            oikein++;

        } else if ($("#visa8c").is(":checked")) {
            luku++;
            $("[id=visa8c]").next().addClass("bg-danger", "text-white");
            $("[id=visa8b]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus8]").prop("disabled", true);
            $("[id=seuraava8]").prop("disabled", false);
            $("[id=visa8a]").attr("disabled", "true");
            $("[id=visa8b]").attr("disabled", "true");
            $("[id=teksti8]").removeClass("blurry-text");
            $("[id=teksti8]").removeClass("disabled");
            $("[id=teksti8]").removeClass("valinta");

        } else {
            $('#virhe').modal('show')
        }

    });
    $("#seuraava8").click(function () {

        $("#visa9").removeClass("not_visible");
        $("#visa8").addClass("not_visible");
    });

    $("#vastaus9").click(function () {

        if ($("#visa9a").is(":checked")) {
            luku++;

            $("[id=visa9a]").next().addClass("bg-danger", "text-white");
            $("[id=visa9c]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus9]").prop("disabled", true);
            $("[id=seuraava9]").prop("disabled", false);
            $("[id=visa9b]").attr("disabled", "true");
            $("[id=visa9c]").attr("disabled", "true");
            $("[id=teksti9]").removeClass("blurry-text");
            $("[id=teksti9]").removeClass("disabled");
            $("[id=teksti9]").removeClass("valinta");


        } else if ($("#visa9b").is(":checked")) {
            luku++;
            $("[id=visa9b]").next().addClass("bg-danger", "text-white");
            $("[id=visa9c]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "red");
            $("[id=vastaus9]").prop("disabled", true);
            $("[id=seuraava9]").prop("disabled", false);
            $("[id=visa9a]").attr("disabled", "true");
            $("[id=visa9c]").attr("disabled", "true");
            $("[id=teksti9]").removeClass("blurry-text");
            $("[id=teksti9]").removeClass("disabled");
            $("[id=teksti9]").removeClass("valinta");

        } else if ($("#visa9c").is(":checked")) {
            luku++;
            $("[id=visa9c]").next().addClass("bg-success", "text-white");
            $("[id=pallo" + luku + "]").css("color", "green");
            $("[id=vastaus9]").prop("disabled", true);
            $("[id=seuraava9]").prop("disabled", false);
            $("[id=visa9a]").attr("disabled", "true");
            $("[id=visa9b]").attr("disabled", "true");
            $("[id=teksti9]").removeClass("blurry-text");
            $("[id=teksti9]").removeClass("disabled");
            $("[id=teksti9]").removeClass("valinta");
            oikein++;
        } else {
            $('#virhe').modal('show')
        }


    });
    $("#seuraava9").click(function () {

        $("#loppuRuutu").removeClass("not_visible");
        $("#visa9").addClass("not_visible");
        $("#tulokset").html(oikein + "/9");

        if (oikein < 2) {
            $("#huonoin").removeClass("not_visible");
        } else if (oikein < 4) {
            $("#huonompi").removeClass("not_visible");
        } else if (oikein < 6) {
            $("#huono").removeClass("not_visible");
        } else if (oikein < 8) {
            $("#ok").removeClass("not_visible");
        } else if (oikein < 9) {
            $("#parempi").removeClass("not_visible");
        } else {
            $("#paras").removeClass("not_visible");
        }
    });

    $("#alusta").click(function () {
        location.reload();

    });

});
