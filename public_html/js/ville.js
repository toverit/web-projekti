/*
    Created on : 26/03/2020
    Author     : Ville Rantanen
    Subject    : Visa
*/

$(document).ready(function(){
//  Globaalit muuttujat
    let visanVaiheet = [
        "#visa_vaihe_0",
        "#visa_vaihe_1",
        "#visa_vaihe_2",
        "#visa_vaihe_3",
        "#visa_vaihe_4",
        "#visa_vaihe_5",
        "#visa_vaihe_6",
        "#visa_vaihe_7",
        "#visa_vaihe_8",
    ];
    
    let visanEteneminen = [
        "",
        "#visa_eteneminen_1",
        "#visa_eteneminen_2",
        "#visa_eteneminen_3",
        "#visa_eteneminen_4",
        "#visa_eteneminen_5",
        "#visa_eteneminen_6",
        "#visa_eteneminen_7",
        "#visa_eteneminen_8",
    ];

    let näkyväVaihe = 0;
    let oikeinMäärä = 0;

//  Valintapainikkeen klikkaus
    $("button.valintapainike").click(function(){
        // minkä kysymyksen valintapainikkeet?
        let kysymys = $(this).attr("name");

        // poistetaan valintapainikkeet käytöstä
        let valinta = "[name=" + kysymys + "]";
        $(valinta).prop("disabled", true);

        // onko vastattu oikein?
        let vastaus = Number($(this).val());
        if (vastaus === 1) {
            // vastattu oikein
            $(this).addClass("oikein_painike");
            $(".oikein_teksti").removeClass("d-none");
            $(visanEteneminen[näkyväVaihe]).removeClass("lukittu");
            $(visanEteneminen[näkyväVaihe]).addClass("oikein_teksti");
            oikeinMäärä++;
        } else {
            // vastattu väärin
            $(this).addClass("väärin_painike");
            $(".väärin_teksti").removeClass("d-none");
            $(visanEteneminen[näkyväVaihe]).removeClass("lukittu");
            $(visanEteneminen[näkyväVaihe]).addClass("väärin_teksti");

            // näytetään oikea vastaus
            let kysymys = $(this).attr("name");
            let oikeaVastaus = "[name=" + kysymys + "][value=1]";
            $(oikeaVastaus).addClass("oikein_painike");
        }

        // näytetään selitys
        $(".selitys").removeClass("d-none");
    });

//  Seuraava-painikkeen klikkaus
    $("button.seuraava").click(function(){
        // näytetään otsikko ja etenemisen seuranta
        $("#otsikkorivi").removeClass("d-none");

        // päivitetään etenemisen seuranta
        $(visanEteneminen[näkyväVaihe]).removeClass("fa-lock");
        $(visanEteneminen[näkyväVaihe]).addClass("fa-lock-open");

        // piilotetaan visan edellinen vaihe
        $(visanVaiheet[näkyväVaihe]).addClass("d-none");
        näkyväVaihe++;

        // piilotetaan oikein- ja väärin-kuvakkeet sekä selitys
        $("div.oikein_teksti").addClass("d-none");
        $("div.väärin_teksti").addClass("d-none");
        $("div.selitys").addClass("d-none");

        // näytetään visan seuraava vaihe
        if (näkyväVaihe < visanVaiheet.length) {
            $(visanVaiheet[näkyväVaihe]).removeClass("d-none");
        } else {
            // näytetään lopetusvaihe
            $("#visa_vaihe_9").removeClass("d-none");
            $("#oikein_määrä").html(oikeinMäärä + "/8");
            if (oikeinMäärä === 0) {
                $("#visa_palkinto_4").removeClass("d-none");
            } else if (oikeinMäärä <= 3) {
                $("#visa_palkinto_3").removeClass("d-none");
            } else if (oikeinMäärä <= 6) {
                $("#visa_palkinto_2").removeClass("d-none");
            } else {
                $("#visa_palkinto_1").removeClass("d-none");
            }
        }
    });

//  Aloita alusta -painikkeen klikkaus
    $("#aloita_alusta").click(function(){
        location.reload();
    });
});