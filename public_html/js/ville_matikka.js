/*
    Created on : 02/05/2020
    Author     : Ville Rantanen
    Subject    : Matematiikkatehtävä
*/

$(document).ready(function(){
    /** Satunnaislukugeneraattori. Arpoo satunnaisen luvun halutulta väliltä.
     *  
     *  @param {number} min     Halutun lukualueen pienin arvo.
     *  @param {number} max     Halutun lukualueen suurin arvo.
     */
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

//  Globaalit muuttujat
    let matikkaKuvat = [
        "",
        "#matikka_kuva_1",
        "#matikka_kuva_2",
        "#matikka_kuva_3",
        "#matikka_kuva_4",
        "#matikka_kuva_5",
        "#matikka_kuva_6",
        "#matikka_kuva_7",
        "#matikka_kuva_8",
        "#matikka_kuva_9",
        "#matikka_kuva_10",
        "#matikka_kuva_11",
        "#matikka_kuva_12",
        "#matikka_kuva_13",
        "#matikka_kuva_14",
        "#matikka_kuva_15",
    ];

    let matikkaEteneminen = [
        "",
        "#matikka_eteneminen_1",
        "#matikka_eteneminen_2",
        "#matikka_eteneminen_3",
        "#matikka_eteneminen_4",
        "#matikka_eteneminen_5",
        "#matikka_eteneminen_6",
        "#matikka_eteneminen_7",
        "#matikka_eteneminen_8",
        "#matikka_eteneminen_9",
        "#matikka_eteneminen_10",
        "#matikka_eteneminen_11",
        "#matikka_eteneminen_12",
        "#matikka_eteneminen_13",
        "#matikka_eteneminen_14",
        "#matikka_eteneminen_15",
    ];

    let matikkaKuvatSisältö = [
        "",
        "1 koira",
        "2 lentokonetta",
        "3 viivaa",
        "4 ikkunaa",
        "5 mustikkaa",
        "6 kananmunaa",
        "7 virvoitusjuomatölkkiä",
        "8 kenkää",
        "9 eläinkeksiä",
        "10 timanttia",
        "11 pääsiäismunaa",
        "12 pääsiäismunaa",
        "13 palloa",
        "14 palloa",
        "15 palloa",
    ];

    let tehtäväMäärä = 0;
    let näytettävänKuvanIndex = 0;
    let oikeinMäärä = 0;
    let eteneminen = 1;

//  Aloita-painikkeen klikkaus
    $("#matikka_aloita").click(function(){
        // kuinka monta tehtävää on valittu?
        tehtäväMäärä = Number($("input[name=matikka_valinta]:checked").val());

        // piilotetaan aloitusvaihe ja näytetään laskuvaihe
        $("#matikka_aloitus").addClass("d-none");
        $("#matikka_otsikko").removeClass("d-none");
        $("#matikka_lasku").removeClass("d-none");

        // näytetään oikea määrä lukkokuvakkeita
        for (let i = 1; i <= tehtäväMäärä; i++) {
            $(matikkaEteneminen[i]).removeClass("d-none");
        }

        // näytetään ensimmäinen kuva
        näytettävänKuvanIndex = getRndInteger(1, matikkaKuvat.length - 1);
        $(matikkaKuvat[näytettävänKuvanIndex]).removeClass("d-none");
    });

//  Tarkista-painikkeen klikkaus
    $("#matikka_tarkista").click(function(){
        // muuttujat
        let lukumäärä = $("#lukumäärä").val();
        let naapuriluku_1 = $("#naapuriluku_1").val();
        let naapuriluku_2 = $("#naapuriluku_2").val();
        let virheviestinLaskenta = 0;

        // piilotetaan mahdollinen virheviesti
        $("#matikka_virheviesti_2").addClass("d-none");

        // tarkistetaan, että tiedot on syötetty
        if (lukumäärä === "" || naapuriluku_1 === "" || naapuriluku_2 === "") {
            $("#matikka_virheviesti_2").removeClass("d-none");
            return;
        } else {
            // laitetaan input-elementit ja Tarkista-painike pois käytöstä
            $("#matikka_tarkista").prop("disabled", true);
            $("#lukumäärä").prop("disabled", true);
            $("#naapuriluku_1").prop("disabled", true);
            $("#naapuriluku_2").prop("disabled", true);
        }

        // tarkistetaan vastaukset
        if (Number(lukumäärä) === näytettävänKuvanIndex) {
            $("#lukumäärä_oikein").removeClass("d-none");
        } else {
            $("#lukumäärä_väärin").removeClass("d-none");
            virheviestinLaskenta++;
        }

        if (Number(naapuriluku_1) === näytettävänKuvanIndex - 1) {
            $("#naapuriluku_1_oikein").removeClass("d-none");
        } else {
            $("#naapuriluku_1_väärin").removeClass("d-none");
            virheviestinLaskenta++;
        }

        if (Number(naapuriluku_2) === näytettävänKuvanIndex + 1) {
            $("#naapuriluku_2_oikein").removeClass("d-none");
        } else {
            $("#naapuriluku_2_väärin").removeClass("d-none");
            virheviestinLaskenta++;
        }

        // jos on vastattu oikein kaikkiin
        if (virheviestinLaskenta === 0) {
            $("#matikka_oikein_teksti").removeClass("d-none");
            $(matikkaEteneminen[eteneminen]).removeClass("lukittu");
            $(matikkaEteneminen[eteneminen]).addClass("oikein_teksti");
            oikeinMäärä++;
        }

        // jos on vastattu väärin yhteen tai useampaan
        if (virheviestinLaskenta > 0) {
            // näytetään oikea vastaus
            $("#matikka_oikea_vastaus").removeClass("d-none");
            $("#matikka_kuva_sisältö").html(matikkaKuvatSisältö[näytettävänKuvanIndex]);
            $("#naapuriluku_1_vastaus").html(näytettävänKuvanIndex - 1);
            $("#naapuriluku_2_vastaus").html(näytettävänKuvanIndex + 1);

            // näytetään Väärin-teksti
            $("#matikka_väärin_teksti").removeClass("d-none");
            $(matikkaEteneminen[eteneminen]).removeClass("lukittu");
            $(matikkaEteneminen[eteneminen]).addClass("väärin_teksti");
        }

        // näytetään Seuraava-painike
        $("#matikka_seuraava").removeClass("d-none");
    });

//  Seuraava-painikkeen klikkaus
    $("#matikka_seuraava").click(function(){
        // jos tehtäviä on vielä jäljellä
        if (eteneminen < tehtäväMäärä) {
            // piilotetaan nykyinen kuva ja näytetään seuraava
            $(matikkaKuvat[näytettävänKuvanIndex]).addClass("d-none");
            näytettävänKuvanIndex = getRndInteger(1, matikkaKuvat.length - 1);
            $(matikkaKuvat[näytettävänKuvanIndex]).removeClass("d-none");

            // päivitetään etenemisen seuranta
            $(matikkaEteneminen[eteneminen]).removeClass("fa-lock");
            $(matikkaEteneminen[eteneminen]).addClass("fa-lock-open");

            // tyhjennetään input-elementit ja otetaan ne käyttöön
            $("#lukumäärä").val("");
            $("#naapuriluku_1").val("");
            $("#naapuriluku_2").val("");

            $("#lukumäärä").prop("disabled", false);
            $("#naapuriluku_1").prop("disabled", false);
            $("#naapuriluku_2").prop("disabled", false);

            // otetaan Tarkista-painike käyttöön
            $("#matikka_tarkista").prop("disabled", false);

            // piilotetaan Oikein-kuvakkeet ja -tekstit
            $("#lukumäärä_oikein").addClass("d-none");
            $("#naapuriluku_1_oikein").addClass("d-none");
            $("#naapuriluku_2_oikein").addClass("d-none");
            $("#matikka_oikein_teksti").addClass("d-none");

            // piilotetaan Väärin-kuvakkeet ja -tekstit
            $("#lukumäärä_väärin").addClass("d-none");
            $("#naapuriluku_1_väärin").addClass("d-none");
            $("#naapuriluku_2_väärin").addClass("d-none");
            $("#matikka_väärin_teksti").addClass("d-none");
            $("#matikka_oikea_vastaus").addClass("d-none");

            // piilotetaan Seuraava-painike
            $("#matikka_seuraava").addClass("d-none");

            eteneminen++;
        // jos tehtäviä ei ole enää jäljellä
        } else {
            // päivitetään etenemisen seuranta
            $(matikkaEteneminen[eteneminen]).removeClass("fa-lock");
            $(matikkaEteneminen[eteneminen]).addClass("fa-lock-open");
            
            // piilotetaan laskuvaihe ja näytetään lopetusvaihe
            $("#matikka_lasku").addClass("d-none");
            $("#matikka_lopetus").removeClass("d-none");
            $("#oikeat_vastaukset_määrä").html(oikeinMäärä);
            $("#tehtävät_määrä").html(tehtäväMäärä);
        }
    });

//  Aloita alusta -painikkeen klikkaus
    $("#matikka_aloita_alusta").click(function(){
        location.reload();
    });
});