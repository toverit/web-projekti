﻿/*
    Created on : 26/03/2020
    Author     : Jenna Pennanen
*/

$(document).ready(function () {
    //Visan jQuery
    let quizzes = ["#quiz1", "#quiz2", "#quiz3", "#quiz4", "#quiz5", "#quiz6", "#quiz7", "#quiz8"];
    let visible = 0;
    visible = getRndInteger(0, quizzes.length - 1);

    //Random-generaattori
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    //Aloituspainikkeen funktio
    $("#start").click(function () {
        $(quizzes[visible]).removeClass("not_visible");
        $(this).addClass("not_visible"); //start-painikkeelle piilotus
        $('input[type="radio"]').prop('checked', false);
    });

    let oikea = 0;

    //Valintapainikkeen funktio
    $(".choice").click(function () {
        let attribuutinArvo = $(this).attr("name");

        let valinta = "[name=" + attribuutinArvo + "]";
        $(valinta).prop("disabled", true);

        let vastaus = Number($(this).val());

        if (vastaus === 1) {
            $(this).parent().addClass("selected").addClass("right");
            oikea++;
            $("#palkinto").removeClass("not_visible");

        } else {
            $(this).parent().addClass("wrong");

            let attribuutinArvo = $(this).attr("name");
            let valinta = "[name=" + attribuutinArvo + "][value=1]";
            $(valinta).parent().addClass("right");
        }

        $("#count_p").removeClass("not_visible");

        $("#" + attribuutinArvo).removeClass("not_visible");
        $("#next").removeClass("not_visible");
    });


    let total = 0;


    //Seuraava-painikkeen funktio
    $("#next").click(function () {
        $("#palkinto").addClass("not_visible");
        $(quizzes[visible]).addClass("not_visible");
        quizzes.splice(visible, 1);

        if (quizzes.length > 0) {
            //jos kysymyksiä vielä jäljellä
            visible = getRndInteger(0, quizzes.length - 1);
            $(quizzes[visible]).removeClass("not_visible");
            $(this).addClass("not_visible");
        } else {
            //mitä tehdään, kun kaikkiin vastattu
            $(this).addClass("not_visible");

            $("#finish").removeClass("not_visible");

            $("#finish").click(function () {
                $(this).addClass("not_visible");
                $("[name=finish2]").removeClass("not_visible");
                $("#correct").html(oikea + " ");
                $("#start_again").removeClass("not_visible");
                $("#count_p").addClass("not_visible");

                if (oikea === 8) {
                    $("#oikein").removeClass("not_visible");
                }
            });
        }

        total++;

        $("#count").html(total + " ");

    });

    $("#start_again").click(function () {
        location.reload();

    });


    //Matikkatehtävän jQuery

    let oikea_vastaus = 0;
    let oikea_vast_count = 0; //oikeiden vastausten m��r�
    let v_vastaus = 0; //v��rien vastausten m��r�

    let t_lukum = 0; //teht�vien valittu lukum��r� (5 - 15)
    let esitetyt = 0; //esitettyjen kysymysten m��r�
    let lukualue = 0; //laskujen lukualue

    let tulos = 0; //luku1 + luku2 vastaus
    let luku1 = 0; //ensimm�inen randomoitu luku
    let luku2 = 0; //toinen randomoitu luku


    //funktio, joka arvoo laskettavat luvut lukualueen mukaan
    function rndNumbers() {

        luku1 = getRndInteger(1, (lukualue));
        luku2 = getRndInteger(1, (luku1 - 1));

        tulos = luku1 - luku2;

        oikea_vastaus = tulos;
    }


    //Aloita-painikkeen funktio
    $("#start2").click(function () {
        $("#begin").addClass("not_visible");
        $("#begin2").addClass("not_visible");
        $("#form").removeClass("not_visible");
        $("#count_p2").removeClass("not_visible");
        $("#tehtq").html("");
        $("#check").removeClass("not_visible");
        $("#ans").val("");
        $("#ans").select();

        if ($("#5").prop("checked") === true) {
            t_lukum = 5;
        } else if ($("#10").prop("checked") === true) {
            t_lukum = 10;
        } else if ($("#15").prop("checked") === true) {
            t_lukum = 15;
        }

        if ($("#ten").prop("checked")) {
            lukualue = 10;
        } else if ($("#twenty").prop("checked")) {
            lukualue = 20;
        } else if ($("#thirty").prop("checked")) {
            lukualue = 30;
        }

        rndNumbers();

        $("#tehtq").html(luku1 + " - " + luku2 + " = ");
    });


    //Tarkista-painikkeen funktio
    $("#check").click(function () {
        let vastaus = Number($("#ans").val());
        $("#error").addClass("not_visible");
        $("#label").removeClass("wrong");

        if (vastaus === oikea_vastaus) {

            $("#palkinto").removeClass("not_visible");
            $("#label").addClass("selected");
            $("#oikea_vastaus2").html(oikea_vast_count);

            $("#next2").removeClass("not_visible");
            $("#check").addClass("not_visible");

            oikea_vast_count++;
            t_lukum--;

        } else {
            v_vastaus++;
            $("#error").removeClass("not_visible");
            $("#label").addClass("wrong");
        }

        $("#count2").html(t_lukum + " ");
    });

    //Valintaruudun klikkausfunktio, joka korostaa syöttökentän sisällön
    $("#ans").click(function () {
        $("#ans").select();
    });

    //Seuraava-painikkeen funktio
    $("#next2").click(function () {
        $("#tehtq").html("");
        $("#next2").addClass("not_visible");
        $("#check").removeClass("not_visible");
        $("#palkinto").addClass("not_visible");
        $("#label").removeClass("selected");

        if (esitetyt < t_lukum) {
            $("#ans").val("");
            $("#palkinto").addClass("not_visible");

            rndNumbers();

            $("#tehtq").html(luku1 + " - " + luku2 + " = ");

        } else {
            $("#form").addClass("not_visible");
            $("#count_p2").addClass("not_visible");
            $("#check").addClass("not_visible");
            $("#oikea_vastaus").removeClass("not_visible");
            $("#v_vastaus").removeClass("not_visible");
            $("#alkuun").removeClass("not_visible");
            $("#next2").addClass("not_visible");

            $("#v_vastaus2").html(v_vastaus);
            $("#oikea_vastaus2").html(oikea_vast_count);

            $("#reward").removeClass("not_visible");

        }

        //asettaa kursorin valmiiksi vastauskenttään
        $("#ans").select();
    });

    //Aloita alusta -painikkeen funktio
    $("#alkuun").click(function () {
        location.reload();
    });

});

